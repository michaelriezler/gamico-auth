# Starting the application

1. The projects relies on the workspaces feature provided by yarn. To make sure everything is installed correctly run `yarn install` from the root directory of the project.   

2. `cd client && npm start`
3. The first time you run the application, make sure the database is setup correctly, inside the server directory run `node ./schema.js`
4. `cd server && npm start`


# Client Development

Inside the client directory:   
1. Start the application `npm start`

2. The client directory includes a dev server that can be used durring the development of the client. Run `node ./server`

## Client Test Data
### Registration
- username: taken => username_taken
- email: taken@gamico.gg => email_taken
- password: badpassword => bad_password

### Login
- email: wrong@gamico.gg => wrong_username_password
- password: wrongpassword => wrong_username_password


# Server Development

Before you start:   
- make sure you have MySQL installed localy (version 5.7)
- optionaly you can use docker to start an MySQL instance
- __set the root password to root__

1. Start your MySQL instance:
   `docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7`

2. `npm start`