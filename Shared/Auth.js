exports.Errors = {
  UsernameTaken: 'username_taken',
  EmailTaken: 'email_taken',
  EmailBad: 'bad_email',
  PasswordBad: 'badpassword',
  WrongUsernamePassword: 'wrong_username_password',
  EmailInvalid: 'email_invalid',
  UsernameLength: 'username_length',
  UsernameInvalid: 'username_invalid',
  PasswordInvalid: 'password_invalid'
}
