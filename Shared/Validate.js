// @flow
// https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
let { Errors } = require('./Auth')

exports.email = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  let ok = re.test(String(email).toLowerCase())
  let errors = ok
    ? []
    : [{
      type: Errors.EmailInvalid,
      msg: 'the email address you proided is not valid'
    }]

  return {
    ok,
    errors,
    value: email
  }
}

let isAlphanumeric = (str) => {
  let re = /^[a-zA-Z0-9]*$/
  return re.test(String(str))
}
exports.username = (username) => {
  let inRange = username.length >= 4 && username.length <= 20

  let errors = []

  if (!inRange) {
    errors.push({
      type: Errors.UsernameLength,
      msg: 'usernames should be between 4 and 20 characters long'
    })
  }

  let alpha = isAlphanumeric(username)
  if (!alpha) {
    errors.push({
      type: Errors.UsernameInvalid,
      msg: 'usernames can only contain letters and numbers'
    })
  }

  return {
    ok: inRange && alpha,
    value: username,
    errors
  }
}

exports.password = (password) => {
  let inRange = password.length >= 8 && password.length <= 20

  let errors = inRange
    ? []
    : [{
      type: Errors.PasswordInvalid,
      msg: 'your password should be between 8 and 20 characters long'
    }]

  return {
    ok: inRange,
    value: password,
    errors
  }
}
