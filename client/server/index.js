let express = require('express')
let bodyParser = require('body-parser')

let app = express()
app.use(bodyParser.json())
app.use(bodyParser.text())

let router = express.Router()

let dummyToken = 'dummy.auth.token'

let handleSignUp = (req, res) => {
  let { username, email, password } = req.body
  let errors = []

  if (username === 'taken') {
    errors.push({ type: 'username_taken', msg: 'Username is already taken' })
  }

  if (email === 'taken@gamico.gg') {
    errors.push({ type: 'email_taken', msg: 'Email adress is already taken' })
  }

  if (email === 'bad@gamico.gg') {
    errors.push({ type: 'bad_email', msg: '' })
  }

  if (password === 'badpassword') {
    errors.push({ type: 'bad_password', msg: '' })
  }

  setTimeout(() => {
    if (errors.length > 0) {
      res.status(400).json(errors.reduce(
        (acc, x) => Object.assign(acc, { [x.type]: x.msg }),
        {}
      ))
    } else {
      res.status(200).json({
        username,
        email,
        token: dummyToken
      })
    }
  }, 1000)
}
router.post('/signup', handleSignUp)

let handleSignIn = (req, res) => {
  let { email, password } = req.body
  let errors = []

  if (email === 'wrong@gamico.gg') {
    errors.push({
      type: 'wrong_username_password',
      msg: 'wrong username or password'
    })
  }

  if (password === 'wrongpassword') {
    errors.push({
      type: 'wrong_username_password',
      msg: 'wrong username or password'
    })
  }

  setTimeout(() => {
    if (errors.length > 0) {
      res.status(400).json(errors.reduce(
        (acc, x) => Object.assign(acc, { [x.type]: x.msg }),
        {}
      ))
    } else {
      res.status(200).json({
        username: 'gamico',
        email,
        token: dummyToken
      })
    }
  }, 1000)
}
router.post('/signin', handleSignIn)

let handleVerify = (req, res) => {
  let token = req.body
  setTimeout(() => {
    if (token === 'dummy.auth.token') {
      res.status(200).send()
    } else {
      res.status(400).send()
    }
  }, 1000)
}
router.post('/verify', handleVerify)
app.use('/api', router)

app.listen(
  4000,
  () => console.log('dev server is running on port: 4000')
)
