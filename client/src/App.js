// @flow
import React, { Component, Children } from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect, 
  Link 
} from 'react-router-dom'
import Auth from './Screen/Auth'
import { onAuthStateChanged, signOut } from './Utils/Auth'
import Wrapper from './Component/Wrapper'
import { Button } from '@rmwc/button'
import { Typography } from '@rmwc/typography'

let Main = () => (
  <Wrapper>
    <Link to='/app'>Private 1</Link>
    <Link to='/app/other'>Private 2</Link>

    <Typography use='headline4'>Private</Typography>
    <Button onClick={signOut}>Sign Out</Button>
  </Wrapper>
)

let Main2 = () => (
  <Wrapper>
    <Link to='/app'>Private 1</Link>
    <Link to='/app/other'>Private 2</Link>

    <Typography use='headline4'>Other Private Page</Typography>
    <Button onClick={signOut}>Sign Out</Button>
  </Wrapper>
)

class App extends Component {

  state = {
    isAuthenticated: false,
    loading: true
  }

  componentDidMount () {
    onAuthStateChanged((user) => {
      let isAuthenticated = user !== null
      this.setState({ isAuthenticated, loading: false })
    })
  }

  Private = props => {
    let children = Children.map(
      props.children,
      node => 
        <Route
          path={node.props.path}
          render={() => node}
        />
    )

    return this.state.isAuthenticated
      ? children
      : <Redirect to='/auth' />
  }

  render() {
    if (this.state.loading) {
      return <div>...loading...</div>
    }
    return (
      <div className="App">
        <Switch>
          <Route path='/auth' component={Auth} />
          <this.Private>
            <Main2 path='/app/other' />
            <Main path='/app' />
          </this.Private>
        </Switch>
      </div>
    )
  }
}

export default () => (
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
