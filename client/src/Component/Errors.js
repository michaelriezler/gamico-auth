import React from 'react'
import styled from 'styled-components'

export let ErrorText = styled.li`
  color: red;
  list-style-type: none;
  font-size: 12px;
`

export let ErrorWrapper = styled.ul`
  margin-top: 0;
  padding-left: 0;
`
export default ({ value, errors }) => {
  if (value.length === 0) return null
  if (errors.length === 0) return null

  return (
    <ErrorWrapper>
      { errors.map(err =>
        <ErrorText key={err.type}>{err.msg}</ErrorText>) }
    </ErrorWrapper>
  )
}
