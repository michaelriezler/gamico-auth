import styled from 'styled-components'

export default styled.form`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`
