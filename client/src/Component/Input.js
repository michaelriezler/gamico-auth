import styled from 'styled-components'
import { TextField } from '@rmwc/textfield'

export default styled(TextField)`
  margin-bottom: 24px;
  background-color: var(--mdc-theme-secondary)!important;
`
