import React from 'react'
import { Button } from '@rmwc/button'
import { CircularProgress } from '@rmwc/circular-progress'

export default ({ children, loading = false, ...props }) => (
  <Button {...props}>
    { loading
      ? <CircularProgress />
      : children
    }
  </Button>
)
