import styled from 'styled-components'
import { AppPadding } from '../Utils/Styles'

export default styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  ${AppPadding}
`
