import React from 'react'
import { Button } from '@rmwc/button'
import styled from 'styled-components'
import { AppPadding } from '../Utils/Styles'
import { Link, Switch, Route, Redirect } from 'react-router-dom'

import SignIn from './SignIn'
import SignUp from './SignUp'

let Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  ${AppPadding}
`

let SignInBtn = styled(Button)`
  margin-top: auto;
`

let StyledLink = styled(Link)`
  color: inherit;
  text-decoration: none;
  width: 100%;
`

let Authentication = () => (
  <Wrapper>

    <SignInBtn raised>
      <StyledLink to='/auth/signin'>Login</StyledLink>
    </SignInBtn>

    <Button>
      <StyledLink to='/auth/signup'>Register</StyledLink>
    </Button>
  </Wrapper>
)

export default () => (
  <Switch>
    <Route path='/auth/signup' component={SignUp} />
    <Route path='/auth/signin' component={SignIn} />
    <Route path='/auth' component={Authentication} />
    <Redirect to='/auth' />
  </Switch>
)
