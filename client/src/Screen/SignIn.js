import React from 'react'
import styled from 'styled-components'
import { Typography } from '@rmwc/typography'
import Wrapper from '../Component/Wrapper'
import Input from '../Component/Input'
import Form from '../Component/Form'
import LoadingButton from '../Component/LoadingButton'
import { signIn } from '../Utils/Auth'
import Validate from 'Shared/Validate'
import Errors, { ErrorWrapper, ErrorText } from '../Component/Errors'

let LoginBtn = styled(LoadingButton)`
  margin-top: auto;
  color: var(--mdc-theme-primary)!important;
  background: var(--mdc-theme-secondary)!important;

  :disabled {
    color: var(--mdc-theme-secondary)!important;
    background: var(--mdc-theme-primary)!important;
  }
`

let StyledWrapper = styled(Wrapper)`
  background: var(--mdc-theme-primary);
`

let Headline = styled(Typography)`
  color: var(--mdc-theme-on-primary);
`

let StyledInput = styled(Input)`
  background: var(--mdc-theme-primary)!important;
  
  input,
  label {
    color: var(--mdc-theme-secondary)!important;
  }
`

class Login extends React.Component {

  state = {
    errors: {},
    email: '',
    password: '',
    loading: false
  }

  handleSubmit = e => {
    e.preventDefault()
    this.setState({ loading: true })
    signIn(this.state.email, this.state.password)
      .then(res => {
        this.setState({ loading: false })
        this.props.history.push('/app')
      })
      .catch(errors => this.setState({ errors, loading: false }) )
  }

  handleChange = ({ target: { name, value } }) =>
    this.setState({ [name]: value })


  Errors = ({ value, errors }) => {
    if (value.length === 0) return null
    if (errors.length === 0) return null

    return (
      <ErrorWrapper>
        { errors.map(err =>
          <ErrorText key={err.type}>{err.msg}</ErrorText>) }
      </ErrorWrapper>
    )
  }

  render () {
    let email = Validate.email(this.state.email)
    let password = Validate.password(this.state.password)

    let login = email.ok && password.ok

    return (
      <StyledWrapper>
        <Headline use='headline4' tag='h1'>
          Login
        </Headline>

        <ErrorWrapper>
          { Object.values(this.state.errors).map(err =>
              <ErrorText>{err}</ErrorText>
          )}
        </ErrorWrapper>

        <Form onSubmit={this.handleSubmit}>
          <StyledInput
            label='Email'
            name='email'
            onChange={this.handleChange}
            invalid={!email.ok && password.value.length > 0}
            value={email.value.slice(0, 50)}
          />

          <Errors {...email} />

          <StyledInput
            type='password'
            label='Password'
            name='password'
            onChange={this.handleChange}
            invalid={!password.ok && password.value.length > 0}
            value={password.value.slice(0, 20)}
          />

          <Errors {...password} />

          <LoginBtn
            raised
            loading={this.state.loading}
            disabled={!login}
            type='submit'>Login</LoginBtn>
        </Form>
      </StyledWrapper>
    )
  }
}

export default Login
