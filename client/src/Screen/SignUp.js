import React from 'react'
import styled from 'styled-components'
import LoadingButton from '../Component/LoadingButton'
import { signUp } from '../Utils/Auth'
import { Typography } from '@rmwc/typography'
import Validate from 'Shared/Validate'
import { Switch, Route, Link } from 'react-router-dom'
import Wrapper from '../Component/Wrapper'
import Input from '../Component/Input'
import Form from '../Component/Form'
import Errors, { ErrorWrapper, ErrorText } from '../Component/Errors'

let Register = styled(LoadingButton)`
  margin-top: auto;

  .rmwc-circular-progress {
    color: var(--mdc-theme-secondary);
  }
`

class SignUpUser extends React.Component {
  state = {
    errors: {},
    username: '',
    email: '',
    password: '',
    loading: false
  }

  handleChange = ({ target: { name, value } }) =>
    this.setState({ [name]: value })

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({ loading: true })
    let { username, email, password } = this.state
    signUp(username, email, password)
      .then(user => {
        this.setState({ loading: false })
        this.props.history.push('/auth/signup/sucess')
        this.props.onSignUp(user)
      })
      .catch(errors => this.setState({ errors, loading: false }))
  }

  render () {
    let { errors } = this.state

    let username = Validate.username(this.state.username)
    let email = Validate.email(this.state.email)
    let password = Validate.password(this.state.password)

    let register = (
      username.ok && email.ok && password.ok
    )

    return (
      <Wrapper>
        <Typography use='headline4' tag='h1'>Register</Typography>

        <ErrorWrapper>
          { Object.values(errors).map(err =>
              <ErrorText>{err}</ErrorText>
          )}
        </ErrorWrapper>

        <Form onSubmit={this.handleSubmit}>
                
          <Input
            type="text"
            label='Username'
            name='username'
            onChange={this.handleChange}
            invalid={!username.ok && username.value.length > 0}
            value={username.value.slice(0, 20)}
          />

          <Errors {...username} />

          <Input
            type="email"
            label='Email'
            name='email'
            onChange={this.handleChange}
            invalid={!email.ok && email.value.length > 0}
            value={email.value.slice(0, 50)}
          />

          <Errors {...email} />

          <Input
            type="password"
            label='Password'
            name='password'
            onChange={this.handleChange}
            invalid={!password.ok && password.value.length > 0}
            value={password.value.slice(0, 20)}
          />

          <Errors {...password} />

          <Register
            raised
            loading={this.state.loading}
            disabled={!register}
            type='submit'>
            Register
          </Register>  
        </Form>
      </Wrapper>
    )
  }
}

let Welcome = styled(Typography)`
  text-align: center;
`

let SignUpSucess = (p) => (
  <Wrapper>
    <Welcome use='headline4' tag='h1'>
      Welcome <br/>
      { p.user.username }
    </Welcome>
    <Link to="/app">Go to App</Link>
  </Wrapper>
)

class SignUp extends React.Component {
  state = {
    user: {}
  }

  handleSignUp = user => {
    this.setState({ user })
  }

  render () {
    return (
      <Switch>
        <Route 
          path='/auth/signup/sucess' 
          render={() => <SignUpSucess user={this.state.user} />}
        />
        <Route
          path='/auth/signup'
          render={props =>
            <SignUpUser
              {...props} 
              onSignUp={this.handleSignUp}
            />
          }
        />
      </Switch>
    )
  }
}

export default SignUp