let authCallback = () => {}

let signInUpCallback = async res => {
  if (res.ok) {
    let user = await res.json()
    localStorage.setItem('user', JSON.stringify(user))
    authCallback(user)
    return user
  }
  let errors = await res.json()
  return Promise.reject(errors)
}

let base = ''

let postData = (url = '', data = {}) =>
  fetch(`${base}${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify(data)
  })

export let signIn = (email, password) =>
  postData('/api/signin', { email, password })
    .then(signInUpCallback)

export let signUp = (username, email, password) =>
  postData('/api/signup', { username, email, password })
    .then(signInUpCallback)

export let signOut = () => {
  localStorage.setItem('user', null)
  authCallback(null)
}

let verifyToken = (token) => fetch('/api/verify', {
  method: 'POST',
  body: token
}).then(res => res.status === 200)

export let onAuthStateChanged = async (fn) => {
  authCallback = fn
  let user = JSON.parse(localStorage.getItem('user'))

  if (user === null) {
    authCallback(null)
    return
  }

  let isValid = await verifyToken(user.token)
  authCallback(isValid ? user : null)
}
