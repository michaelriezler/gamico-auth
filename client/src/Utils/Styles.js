
export let Const = {
  baseline: 8
}

export let AppPadding = `
  padding: ${Const.baseline * 4}px;
`
