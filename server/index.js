let express = require('express')
let bodyParser = require('body-parser')
let Validate = require('Shared/Validate')
let { Errors } = require('Shared/Auth')
let User = require('./user')
let bcrypt = require('bcrypt')
let jwt = require('jsonwebtoken')

let oneHoure = Math.floor(Date.now() / 1000) + (60 * 60)
let secret = 'jwtsecret'

let saltRounds = 10
let app = express()

let toObject = (acc, x) => Object.assign(acc, { [x.type]: x.msg })

app.use(bodyParser.json())
app.use(bodyParser.text())

let router = express.Router()
router.post('/signin', async (req, res) => {
  let { body } = req

  let email = Validate.email(body.email)
  let password = Validate.password(body.password)

  let errors = [
    ...email.errors,
    ...password.errors
  ]

  let [user] = await User.getByEmail(email.value)

  if (user === null) {
    errors.push({
      type: 'wrong_username_password',
      msg: 'wrong username or password'
    })

    return res.status(400).json(errors.reduce(toObject, {}))
  }

  let match = await bcrypt.compare(
    password.value,
    user.password.toString('utf8')
  )

  if (!match) {
    errors.push({
      type: 'wrong_username_password',
      msg: 'wrong username or password'
    })

    return res.status(400).json(errors.reduce(toObject, {}))
  }

  let token = jwt.sign({ exp: oneHoure }, secret)
  res.status(200).json({
    username: user.username,
    email: user.email,
    token
  })
})

router.post('/signup', async (req, res) => {
  let { body } = req
  let username = Validate.username(body.username)
  let password = Validate.password(body.password)
  let email = Validate.email(body.email)
  let errors = [
    ...email.errors,
    ...username.errors,
    ...password.errors
  ]

  let [usernameTaken, emailTaken] = await Promise.all([
    User.getByUsername(username.value),
    User.getByEmail(email.value)
  ])
    .catch(err => {
      console.log('Error', err)
      return []
    })

  if (usernameTaken !== null) {
    errors.push({
      type: Errors.UsernameTaken,
      msg: 'Username is already taken'
    })
  }

  if (emailTaken !== null) {
    errors.push({
      type: Errors.EmailTaken,
      msg: 'Email adress is already taken'
    })
  }

  if (errors.length > 0) {
    return res
      .status(400)
      .json(errors.reduce(toObject, {}))
  }

  let hashedPassword = await bcrypt.hash(password.value, saltRounds)

  await User.insert(
    username.value,
    email.value,
    hashedPassword
  )

  let token = jwt.sign({ exp: oneHoure }, secret)

  res.status(200).json({
    username: username.value,
    email: email.value,
    token
  })
})

router.post('/verify', (req, res) => {
  let token = req.body

  try {
    var decoded = jwt.verify(token, secret)
    res.status(200).send()
  } catch (err) {
    res.status(400).send()
  }
})

app.use('/api', router)

app.listen(4000, () => console.log('Server is running on port 4000'))
