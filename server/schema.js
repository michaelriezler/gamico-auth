let mysql = require('mysql')

let pool = mysql.createConnection({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: 'root'
})

let query = query => new Promise((resolve, reject) => {
  pool.query(query, (error, results, fields) => {
    if (error !== null) {
      return reject(error)
    }

    resolve(results || null)
  })
})

let DB = 'Gamico'

let CreateDatabase = `
  CREATE DATABASE IF NOT EXISTS ${DB};
`

let CreateTable = `
  CREATE TABLE IF NOT EXISTS Users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL,
    /* https://stackoverflow.com/a/5882472 */
    password BINARY(60) NOT NULL ,
    PRIMARY KEY (id)
  );
`

async function main () {
  try {
    console.log(`Create Database: ${DB}`)
    await query(CreateDatabase)
    console.log(`Use Database: ${DB}`)
    await query(`USE ${DB}`)
    console.log('Create Users Table')
    await query(CreateTable)
    console.log('Done!')
  } catch (err) {
    console.log(err)
  }
}

main()
