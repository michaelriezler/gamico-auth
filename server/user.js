let mysql = require('mysql')

let pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'Gamico'
})

let query = query => new Promise((resolve, reject) => {
  pool.query(query, (error, results, fields) => {
    if (error !== null) {
      return reject(error)
    }
    resolve(results.length === 0 ? null : results)
  })
})

exports.getByEmail = email => query(`
    SELECT *
    FROM Users
    WHERE email = ${mysql.escape(email)};
  `)

exports.getByUsername = username => query(`
    SELECT *
    FROM Users
    WHERE username = ${mysql.escape(username)};
  `)

exports.insert = (username, email, password) => query(`
    INSERT INTO Users
      ( username
      , email
      , password
      )
    VALUES
      ( ${mysql.escape(username)}
      , ${mysql.escape(email)}
      , ${mysql.escape(password)}
      )
  `)
